export class Stock {
	id: number;
	name: string;
	quantity: number;
	currentPrice: number;
	lastUpdate: string;
}
