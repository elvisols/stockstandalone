##	Standalone Stock App

 This is a sample standalone stock app.

 The client is an Angular2 frontend with spring-boot at the background.

- All endpoints are exposed on (**Port: 8181**).


######Application Requirements:
- Java 8
- Maven 3+

######Deploying and Running application:
> This should be done from the root directory.
```
~$ mvn clean package spring-boot:run
```
Once the service is up, the application can be accessed on port 8181.
`http://localhost:8181`
				
##   Cheers! 

