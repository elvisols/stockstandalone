insert into stocks(id,name,quantity,current_price,created,last_update)
values(1, 'Noodles', 234, 150.2, '2018-03-10', '2018-03-10');

insert into stocks(id,name,quantity,current_price,created,last_update)
values(2, 'Green Apple', 17, 60, '2018-03-09', '2018-03-09');

insert into stocks(id,name,quantity,current_price,created,last_update)
values(3, 'Egg', 76, 1.54, '2018-02-21', '2018-02-21');

insert into stocks(id,name,quantity,current_price,created,last_update)
values(4, 'Ankara Agbada', 2, 1420.0, '2018-03-01', '2018-03-01');

insert into stocks(id,name,quantity,current_price,created,last_update)
values(5, 'Santana', 0, 17.0, '2018-01-30', '2018-01-30');