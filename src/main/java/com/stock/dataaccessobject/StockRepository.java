package com.stock.dataaccessobject;

import org.springframework.data.repository.CrudRepository;

import com.stock.domainobject.StockDO;

// Use JPA Data for basic Crud Jobs
public interface StockRepository extends CrudRepository<StockDO, Long> {

}
