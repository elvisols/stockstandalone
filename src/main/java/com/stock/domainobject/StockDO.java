package com.stock.domainobject;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "stocks")
@NamedQueries( {
	@NamedQuery(name = "StockDO.findAll", query = "select s from StockDO s order by s.id ")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockDO {
	
	@SequenceGenerator(name="stock_id_gen",  sequenceName="stock_id_seq", initialValue = 6, allocationSize = 1)
	@Id	@GeneratedValue(generator="stock_id_gen")
	private Long id;
	
	@NotNull(message = "Name parameter missing!")
	@Size(min = 2, message = "Name must have minimum of 2 characters!")
	@Column(length = 100, unique = true, nullable = false)
	private String name;
	
	@NotNull(message="Quantity parameter missing!")
	@Digits(integer=11, fraction=0, message="Quantity number exceeded!")
	private Integer quantity;
	
	@NotNull(message="Current price parameter missing!")
	@Digits(integer=11, fraction=2, message="Current price exceeded!")
	@Column(nullable = false)
	private BigDecimal currentPrice;
	
	@Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime created;
	
	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime lastUpdate = ZonedDateTime.now();

	public StockDO() { }

	public StockDO(@NotNull(message = "Name parameter missing!") String name,
			@NotNull(message = "Quantity parameter missing!") @Digits(integer = 11, fraction = 0, message = "Quantity number exceeded!") Integer quantity,
			@NotNull(message = "Current price parameter missing!") @Digits(integer = 11, fraction = 2, message = "Current price exceeded!") BigDecimal currentPrice) {
		this.name = name;
		this.quantity = quantity;
		this.currentPrice = currentPrice;
		this.created = ZonedDateTime.now();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(ZonedDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentPrice == null) ? 0 : currentPrice.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockDO other = (StockDO) obj;
		if (currentPrice == null) {
			if (other.currentPrice != null)
				return false;
		} else if (!currentPrice.equals(other.currentPrice))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}
	
}
