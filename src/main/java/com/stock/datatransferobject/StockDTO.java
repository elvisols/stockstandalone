package com.stock.datatransferobject;

import java.math.BigDecimal;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockDTO {

	private Long id;
	
	@NotNull(message = "Name parameter missing!")
	private String name;
	
	@NotNull(message="Quantity parameter missing!")
	@Digits(integer=11, fraction=0, message="Quantity number exceeded!")
	private Integer quantity;
	
	@NotNull(message="Current price parameter missing!")
	@Digits(integer=11, fraction=2, message="Current price exceeded!")
	private BigDecimal currentPrice;
	
	private String lastUpdate;

	public StockDTO() { };
	
	public StockDTO(Long id, @NotNull(message = "Name parameter missing!") String name,
			@NotNull(message = "Quantity parameter missing!") @Digits(integer = 11, fraction = 0, message = "Quantity number exceeded!") Integer quantity,
			@NotNull(message = "Current price parameter missing!") @Digits(integer = 11, fraction = 2, message = "Current price exceeded!") BigDecimal currentPrice,
			String lastUpdate) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.currentPrice = currentPrice;
		this.lastUpdate = lastUpdate;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}
	
	public static StockDTOBuilder newBuilder() {
		return new StockDTOBuilder();
	}
	
	public static class StockDTOBuilder {
		
		private Long id;
		private String name;
		private Integer quantity;
		private BigDecimal currentPrice;
		private String lastUpdate;
		
		public StockDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}

		public StockDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public StockDTOBuilder setQuantity(Integer quantity) {
			this.quantity = quantity;
			return this;
		}

		public StockDTOBuilder setCurrentPrice(BigDecimal currentPrice) {
			this.currentPrice = currentPrice;
			return this;
		}

		public StockDTOBuilder setLastUpdate(String lastUpdate) {
			this.lastUpdate = lastUpdate;
			return this;
		}

		public StockDTO createStockDTO() {
			return new StockDTO(id, name, quantity, currentPrice, lastUpdate);
		}
	}
	
}
