package com.stock.service;

import java.util.List;

import com.stock.domainobject.StockDO;
import com.stock.exception.ConstraintsViolationException;
import com.stock.exception.StockNotFoundException;

public interface StockService {

	StockDO findStockById(Long stockId) throws StockNotFoundException;

	StockDO createStock(StockDO stockDO) throws ConstraintsViolationException;
    
	StockDO updateStock(StockDO stockDO) throws ConstraintsViolationException, StockNotFoundException;

    void deleteStock(Long stockId) throws StockNotFoundException;
    
    List<StockDO> findAllStocks();
    
}
