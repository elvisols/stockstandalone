package com.stock.service;

import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.dataaccessobject.StockRepository;
import com.stock.domainobject.StockDO;
import com.stock.exception.ConstraintsViolationException;
import com.stock.exception.StockNotFoundException;


@Service
public class StockServiceImpl implements StockService {

	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(StockServiceImpl.class);

    @Autowired
    private StockRepository stockRepository; 
    
	@Override
	public StockDO findStockById(Long stockId) throws StockNotFoundException {
		LOG.info("...Getting stock with Id: ", stockId);
		return stockRepository.findById(stockId).orElseThrow(() -> new StockNotFoundException("Oops! Could not find stock entity with id:" + stockId));
	}

	@Override
	public StockDO createStock(StockDO stockDO) throws ConstraintsViolationException {
		LOG.info("...Saving stock: ", stockDO);
		return stockRepository.save(stockDO);
	}

	@Override
	public StockDO updateStock(StockDO stockDO) throws ConstraintsViolationException, StockNotFoundException {
		LOG.info("...Updating: ", stockDO);
		StockDO s = this.findStockById(stockDO.getId());
		// Update stock's records accordingly.
		s.setName(stockDO.getName() == null ? s.getName() : stockDO.getName());
		s.setCurrentPrice(stockDO.getCurrentPrice() == null ? s.getCurrentPrice() : stockDO.getCurrentPrice());
		s.setQuantity(stockDO.getQuantity() == null ? s.getQuantity() : stockDO.getQuantity());
		s.setLastUpdate(ZonedDateTime.now());
		LOG.info("...Updating stock with id ", stockDO.getId());
		return stockRepository.save(s);
	}

	@Override
	public void deleteStock(Long stockId) throws StockNotFoundException {
		LOG.warn("...Deleting stock with Id: ", stockId);
		stockRepository.deleteById(stockId);
	}

	@Override
	public List<StockDO> findAllStocks() {
		LOG.info("...Fetching stocks");
		return (List<StockDO>) stockRepository.findAll();
	}

}
