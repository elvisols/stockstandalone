package com.stock.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.stock.controller.mapper.StockMapper;
import com.stock.datatransferobject.StockDTO;
import com.stock.domainobject.StockDO;
import com.stock.exception.ConstraintsViolationException;
import com.stock.exception.StockNotFoundException;
import com.stock.service.StockService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * A class to handle all API Stock requests.
 * This class exposes the STOCK data repository as REST.
 * 
 * <pre>
 *    Allowed Methods: GET, POST, PUT, DELETE
 * </pre>
 * 
 * @author  Elvis
 * @version 1.0, 9/03/18
 */
@RestController
@RequestMapping("api/stocks")
@Api(value = "Stock")
public class StockController {

	@Autowired
	StockService stockService;
	
	/**
	* This method fetches all stocks
	*
	* @param void
	* @method GET
	* @return result as list of stock object
	*/
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all Stocks")
	public List<StockDTO> findStocks() {
		return StockMapper.makeStockDTOList(stockService.findAllStocks());
	}
	
	/**
	* This method returns a single stock record.
	*
	* @param <b>stockId</b> this is the id of the stock record to retrieve.
	* @method GET
	* @return result, as a stock object
	* @throws StockNotFoundException, if stock record for the specified id cannot be retrieved.
	*/
	@GetMapping(value="/{stockId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a single stock by id")
	public StockDTO findStockById(@PathVariable Long stockId) throws StockNotFoundException {
		return StockMapper.makeStockDTO(stockService.findStockById(stockId));
	}
	
	/**
	* This method returns a newly created stock record.
	*
	* @param <b>stockDTO</b> this is the JSON stock object to create.
	* @method POST
	* @return result, as a stock object
	* @throws ConstraintsViolationException, if JSON stock object violates some constraints.
	*/
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a stock")
	public StockDTO addStock(@Valid @RequestBody StockDTO stockDTO) throws ConstraintsViolationException {
		StockDO stock = StockMapper.makeStock(stockDTO);
		return StockMapper.makeStockDTO(stockService.createStock(stock));
	}
	
	/**
	* This method returns a single updated stock record.
	*
	* @param <b>stockId</b> this is the id of the stock record to update.
	* @param <b>stockDTO</b> this is the JSON stock object to update.
	* @method PUT
	* @return result, as a stock object
	* @throws StockNotFoundException | ConstraintsViolationException, if stock record for the specified id cannot be retrieved or constraint(s) is violated.
	*/
	@PutMapping(value="/{stockId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a stock with the specified id")
	public StockDTO updateStock(@Valid @PathVariable long stockId, @RequestBody StockDTO stockDTO) throws ConstraintsViolationException, StockNotFoundException {
		StockDO stock = StockMapper.makeStock(stockDTO);
		stock.setId(stockId);	// set stock id to update
		return StockMapper.makeStockDTO(stockService.updateStock(stock));
	}
	
	/**
	* This method "hard" deletes a single stock record.
	*
	* @param <b>stockId</b> this is the id of the stock record to delete.
	* @method DELETE
	* @return void
	* @throws StockNotFoundException if stock record for the specified id cannot be retrieved.
	*/
	@DeleteMapping(value="/{stockId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete a stock with a particular id")
	public void deleteStock(@Valid @PathVariable long stockId) throws StockNotFoundException {
		stockService.deleteStock(stockId);
//		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted stock with an ID: " + stockId), HttpStatus.OK);
	}
	
}
