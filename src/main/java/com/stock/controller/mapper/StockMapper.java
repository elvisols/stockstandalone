package com.stock.controller.mapper;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.stock.datatransferobject.StockDTO;
import com.stock.domainobject.StockDO;


/**
 * This class provides a Mapper between the stock's data object (DO) and the data transfer object (DTO).
 * 
 * @author  Elvis
 * @version 1.0, 9/03/18
 */
public class StockMapper {

	/**
	* This method maps the stock's DTO to DO.
	*
	* @param <b>stockDTO</b> this is the stock object gotten from client/consumer.
	* @return StockDO
	*/
    public static StockDO makeStock(StockDTO stockDTO)
    {
        StockDO stock = new StockDO(stockDTO.getName(), stockDTO.getQuantity(), stockDTO.getCurrentPrice());
        return stock;
    }
    
    /**
	* This method maps the stock's DO to DTO.
	*
	* @param <b>stockDO</b> this is the stock object gotten from the application.
	* @return StockDTO
	*/
    public static StockDTO makeStockDTO(StockDO stock)
    {
        StockDTO.StockDTOBuilder stockDTOBuilder = StockDTO.newBuilder()
            .setId(stock.getId())
            .setName(stock.getName())
            .setQuantity(stock.getQuantity())
            .setCurrentPrice(stock.getCurrentPrice())
            .setLastUpdate(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a").format(stock.getLastUpdate()));

        return stockDTOBuilder.createStockDTO();
    }
    
    /**
	* This method creates a collection of all stocks DTO.
	*
	* @param <b>stocks</b> this is the stock object collection from the application.
	* @return List<StockDTO>, this streams and maps all stocks in the collection to DTO and return the list to client/consumer.
	*/
    public static List<StockDTO> makeStockDTOList(Collection<StockDO> stocks)
    {
        return stocks.stream()
            .map(StockMapper::makeStockDTO)
            .collect(Collectors.toList());
    }
    
}
