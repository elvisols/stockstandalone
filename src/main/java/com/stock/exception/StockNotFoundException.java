package com.stock.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A class that captures exception thrown when a stock entity is missing in the database.
 *
 * @author  Elvis
 * @version 1.0, 9/3/18
 * @see     java.lang.Exception
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Oops! Could not find stock entity with id")
public class StockNotFoundException extends Exception
{
    static final long serialVersionUID = -3387516993334229948L;

    /**
	* This constructor allows you to override the default message on missing stock exception thrown
	*
	* @param <b>message</b> this is the exception message
	* @exception StockNotFoundException
	*/
    public StockNotFoundException(String message)
    {
        super(message);
    }

}
