package com.stock.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A class that captures exception thrown when a stock object constraint in violated.
 *
 * @author  Elvis
 * @version 1.0, 9/03/18
 * @see     java.lang.Exception
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Oops! Some constraints are violated ...")
public class ConstraintsViolationException extends Exception {

    static final long serialVersionUID = -3387516993224229948L;

    /**
	* This constructor allows you to override the default message on constraint violation exception thrown
	*
	* @param <b>message</b> this is the exception message
	* @exception ConstraintsViolationException
	*/
    public ConstraintsViolationException(String message)
    {
        super(message);
    }

}
