package com.stock.exception;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A class that captures application wide exception.
 *
 * @author  Elvis
 * @version 1.0, 9/03/18
 * @see     org.springframework.web.bind.annotation.ControllerAdvice
 */
@ControllerAdvice
public class GlobalExceptionsHandler {
	
	/**
	 * Handle application wide ConstraintViolationException exception.
	 * 
	 * @return ResponseEntity<Map<K, V>> of exception detail. K = 'message' | 'reason' | 'path' | 'error' | 'status' | 'timestamp'
	 * @exception  ConstraintViolationException
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	public @ResponseBody Object handleCustomException(ConstraintViolationException cVe, HttpServletRequest request) {
		
		System.out.println("Exception caught: " + cVe.getMessage());
		
		String err = "";
		for(ConstraintViolation cveStr: cVe.getConstraintViolations()) {
			err += cveStr.getPropertyPath()+"{"+cveStr.getInvalidValue()+":"+cveStr.getMessage()+"},";
		}
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Oops! Bad or incomplete parameters.");
		errorMsg.put("reason", err);
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", cVe.getMessage());
		errorMsg.put("status", Integer.valueOf(400));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.BAD_REQUEST);
 
	}
	
	/**
	 * Handle application wide StockNotFoundException exception.
	 *
	 * @return ResponseEntity<Map<K, V>> of exception detail. K = 'message' | 'reason' | 'path' | 'error' | 'status' | 'timestamp'
	 * @exception  StockNotFoundException
	 */
	@ExceptionHandler(StockNotFoundException.class)
	public  ResponseEntity<Map<String, Object>> handleMediaException(StockNotFoundException se, HttpServletRequest request) {
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", se.getMessage());
		errorMsg.put("reason", se.getCause().getMessage());
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", "Stock Not Found Exception");
		errorMsg.put("status", HttpStatus.NOT_FOUND.value());
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.NOT_FOUND);
		
	}
	
	/**
	 * Handle application wide Generic/Undefined exception.
	 *
	 * @return ResponseEntity<Map<K, V>> of exception detail. K = 'message' | 'reason' | 'path' | 'error' | 'status' | 'timestamp'
	 * @exception  Exception
	 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody Object handleGeneralException(HttpServletRequest request, Exception e) throws Exception {
		
		System.out.println("Exception message: " + e.getMessage());
		e.printStackTrace();
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Oops! something went wrong...");
		errorMsg.put("reason", "Uncaught internal error !");
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", e.getMessage());
		errorMsg.put("status", Integer.valueOf(500));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
