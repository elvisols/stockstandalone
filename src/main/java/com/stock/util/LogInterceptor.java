package com.stock.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * A class that intercept application wide requests. E.g for logging, filtering etc
 *
 * @author  Elvis
 * @version 1.0, 9/03/18
 * @see     org.springframework.web.servlet.handler.HandlerInterceptorAdapter
 */
public class LogInterceptor extends HandlerInterceptorAdapter {

    private static final Log LOG = LogFactory.getLog(LogInterceptor.class);

    /**
	 * Intercepts application wide requests after completion, before responding to the client/consumer.
	 * 
	 * @return void
	 */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {
        StringBuilder logMessage = new StringBuilder();
        logMessage.append("method: ").append(request.getMethod()).append("\t");
        logMessage.append("uri: ").append(request.getRequestURI()).append("\t");
        logMessage.append("status: ").append(response.getStatus()).append("\t");
        logMessage.append("remoteAddress: ").append(request.getRemoteAddr()).append("\t");

        if (ex != null) {
            LogInterceptor.LOG.error(logMessage.toString(), ex);
        } else {
            LogInterceptor.LOG.info(logMessage.toString());
        }

    }

}
