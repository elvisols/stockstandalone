package com.payconiq.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.stock.StockApiApplication;
import com.stock.domainobject.StockDO;
import com.stock.exception.ConstraintsViolationException;
import com.stock.exception.StockNotFoundException;
import com.stock.service.StockService;


/**
 * This class implements basic integration tests, for the Stock Service.
 *
 * @author  Elvis
 * @version 1.0, 11/03/18
 * @see     org.springframework.web.bind.annotation.ControllerAdvice
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockApiApplication.class)
public class StockServiceTest {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StockService stockService;
	
	@Autowired
    private Validator validator;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void findStockByIdTest() throws StockNotFoundException {
		StockDO stockDO = stockService.findStockById(2L);
		logger.info("findStockByIdTest {}", stockDO.getId());
		assertEquals("Green Apple", stockDO.getName());
		assertEquals(Integer.valueOf("17"), stockDO.getQuantity());
	}
	
	@Test
	public void findStockByIdStockNotFoundExceptionTest() throws StockNotFoundException {
		
        thrown.expect(StockNotFoundException.class);

        thrown.expectMessage(containsString("Oops! Could not find stock entity with id"));
        
		StockDO stockDO = stockService.findStockById(6L); // Specify a non existent id
		logger.info("Stock Not Found Exception {}", stockDO);
	}
	
	@Test
	public void findAllStocksTest() {
		List<StockDO> allStocks = stockService.findAllStocks();
		assertThat(allStocks, hasSize(5));
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void createStockTest() throws ConstraintsViolationException {
		StockDO stockDO = stockService.createStock(new StockDO("Oral B", 8, BigDecimal.valueOf(250.1)));
		assertEquals("Oral B", stockDO.getName());
		assertEquals(Integer.valueOf("8"), stockDO.getQuantity());
		assertEquals(BigDecimal.valueOf(250.1), stockDO.getCurrentPrice());
		logger.info("Stock {} created at " + stockDO.getCreated(), stockDO.getName());
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void createStockConstraintsViolationExceptionTest() throws ConstraintsViolationException {
		StockDO stockDO = stockService.createStock(new StockDO(null, null, null));
		assertForConstraintsViolation(stockDO);
		logger.info("Stock {} created at " + stockDO.getCreated(), stockDO.getName());
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void updateStockTest() throws ConstraintsViolationException, StockNotFoundException {
		// get a stock to update
		StockDO stockDO = new StockDO("Egg Roll", 5, BigDecimal.valueOf(1.54));
		logger.info("Existing Stock {} created at {} and updated at {}", stockDO.getName(), stockDO.getCreated(), stockDO.getLastUpdate());
		// update details
		stockDO.setId(3L);
		stockService.updateStock(stockDO);

		// assert new values
		StockDO stockDO1 = stockService.findStockById(3L);
		logger.info("Updated Stock {} created at {} and updated at {}", stockDO1.getName(), stockDO1.getCreated(), stockDO.getLastUpdate());
		assertEquals("Egg Roll", stockDO1.getName());
		assertEquals(Integer.valueOf("5"), stockDO1.getQuantity());
		assertEquals(BigDecimal.valueOf(1.54), stockDO1.getCurrentPrice());
	}
	
	@Test()
	@Transactional
	public void updateStockConstraintsViolationExceptionTest() {
		
		// update a record with a null name
		StockDO stockDO1 =  new StockDO(null, Integer.valueOf("5"), BigDecimal.valueOf(1.0));
		stockDO1.setId(5L);
		assertForConstraintsViolation(stockDO1);
		
		// update a record with a character name less than 2 characters
		StockDO stockDO2 =  new StockDO("N", Integer.valueOf("5"), BigDecimal.valueOf(1.0));
		stockDO2.setId(5L);
		assertForConstraintsViolation(stockDO2);
        
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void updateStockNotFoundExceptionTest() throws ConstraintsViolationException, StockNotFoundException {
		
		thrown.expect(StockNotFoundException.class);

        thrown.expectMessage(containsString("Oops! Could not find stock entity with id"));
        
		// update a non existent record
		StockDO stockDO =  new StockDO("Not avail", Integer.valueOf("1"), BigDecimal.valueOf(1.0));
		stockDO.setId(6L);
		stockService.updateStock(stockDO);
		
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void deleteStockTest() throws StockNotFoundException {
		// Delete a stock!
		stockService.deleteStock(5L);
		
		thrown.expect(StockNotFoundException.class);

        thrown.expectMessage(containsString("Oops! Could not find stock entity with id"));
        
		StockDO stockDO = stockService.findStockById(5L); // Specify a non existent id
		logger.info("Stock entity deleted {}", stockDO);
	}

	public void assertForConstraintsViolation(StockDO s) {
		Set<ConstraintViolation<StockDO>> violations = validator.validate(s);
		for(ConstraintViolation<StockDO> v: violations) {
			logger.info("Constraint violation!...[{}] => {}", v.getInvalidValue(), v.getMessage());
		}
		assertFalse(violations.isEmpty());
	}
	
}
